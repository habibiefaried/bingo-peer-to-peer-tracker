#include <iostream>
#include "ServerSocket.h"
#include "SocketException.h"
#include <string>
#include <pthread.h>
#include "Konversi.h"
#include "listpemain.h"
#include <stdlib.h>

using namespace std;

ServerSocket server(5051); //listening socket server (global initialization)

void *Room(void *args)
{
	int i;
	int IDServer;
	
	ServerSocket client;
	string data,dataID,Jenis,port,Port;
	string reply;
	string retr; //mengembalikan list of server
	char IPAddress[100];
	char IPDest[100];
	string IPA; //IPAddress char dalam bentuk string
	
	sockaddr_in sockaddress;
	
	i = (int)args;
	
	cout<<"Listening. . .("<<i<<")"<<endl;
	server.accept(client); //jika telah diterima oleh server
	ListPemain LP; //langsung memanggil kelas List of Pemain
  	
  	reply = "Anda pelanggan ke- " + IntToString(i) + ".\nAda " + IntToString(ListPemain::GetNList()) + " orang yang online saat ini.";
  	
  	sockaddress = server.GetSockAddress(); //Get sockaddr dari client 
  	strcpy(IPAddress,inet_ntoa(sockaddress.sin_addr));//mengambil IP address client yang terkoneksi
  	IPA = inet_ntoa(sockaddress.sin_addr);
  	
  	try
	{
		client >> data; //receive dahulu data
		client << reply; //send
	}
	catch ( SocketException& e)
	{
		cout<<"Program menangkap eksepsi : "<<e.description()<<endl;
	};
	
	client >> Jenis; //listening untuk menerima kalimat terakhir (untuk server)
	client >> port;  //Add : Listening untuk menerima port
	
	LP.AddList(i,IPAddress,data,Jenis,port); //Memanggil Method AddList dengan parameter (ID,IP,Nama,Jenis)
	
	ClearScreen();
	ListPemain::PrintList(); //Memanggil method static printlist.
	//Data member kelas ListPemain adalah static juga
	
	cout<<"User bernama "<<data<<" baru saja login dari "<<IPAddress<<"."<<endl;
	cout<<endl<<"bash> ";
	
	if (Jenis == "Client") //jika client
	{
		List Temp;
		Temp = LP.GetList(); //Mengambil List of Server yang online
		while (Temp != NULL)
		{
			if (Temp->Info.Jenis == "Server")
			{
				retr = IntToString(Temp->Info.ID) + " -- " + Temp->Info.Nama;
				client << retr; //mengirimkan List of Alive Server kepada client
				client >> data; //menerima kembalian
			}
			Temp = Temp->Next;
		}
		client << "SELESAI"; //tanda selesai mengirim

		client >> dataID; //receive data tambahan
		
		while (dataID == "-1") //artinya minta request ulang
		{
			Temp = LP.GetList(); //Mengambil List of Server yang online
			while (Temp != NULL)
			{
				if (Temp->Info.Jenis == "Server")
				{
					retr = IntToString(Temp->Info.ID) + " -- " + Temp->Info.Nama;
					client << retr; //mengirimkan List of Alive Server kepada client
					client >> data; //menerima kembalian
				}
				Temp = Temp->Next;
			}
			client << "SELESAI"; //tanda selesai mengirim

			client >> dataID; //receive data tambahan
		}
		
		IDServer = StringToInt(dataID); //ID server pilihan client
		if (LP.SearchIP(IDServer,IPDest)) //Memanggil method "SearchIP"
		{
			client << IPDest;
			if (LP.SearchPort(IDServer,Port))
			{
				client << Port;
			}
			else
				client << "0";
		}
		else
		{
			client << "0"; // 0 merupakan invalid
			client << "0";
		}
		client >> data;
		client << IPA;
	}
	else //jika server
	{
		client << IPA;
	}
	
	try
	{
		client >> data; //menerima data change atau nomor server
		int ID = StringToInt(data);
		if (data != "QUIT")
		{
			client << "OK";
			client >> data;
			string IP = data;
			client << "OK";
			client >> data;
			string Port = data;
			client << "OK";
			LP.EditList(ID,IP,Port);
			
			client >> data; //menerima data dummy
		}
	}
	catch (SocketException& e)
	{
		cout<<"Program menangkap eksepsi : "<<e.description()<<endl;
	}
	cout<<endl<<"bash> ";
	LP.DelList(i); //delete yang sudah keluar, sekaligus panggil DTor otomatis
	pthread_exit(NULL);
}

void *console(void *args)
{
	bool IsStop = false;
	string command;
	
	while (!IsStop)
	{
		cout<<"bash> "; cin>>command;
		if (command == "stopbash")
			IsStop = true;
		else if (command == "clear")
			system("clear");
		else if (command == "lihatpemain")
		{
			system("clear");
			ListPemain::PrintList();
		}else
			cout<<"Perintah salah"<<endl;
	}
}


int main (int argc, char *argv[])
{
	pthread_t threads[10000]; //didefinisikan sebagai array of thread
	int i = 0,rc;
	int JmlThread;
	
	pthread_t con;
	
	cout<<"Masukkan jumlah thread : "; cin>>JmlThread;
	
  // Create the socket
	i = 0;	
  	while (i<JmlThread)
	{
  		rc = pthread_create(&threads[i], NULL, Room, (void*)i); //memanggil thread
  		i++;
  		sleep(0.5);
	}

	ClearScreen();
	rc = pthread_create(&con,NULL,console,(void*)0);
	cout<<"Listening . . ."<<endl;
    pthread_exit(NULL);
  	return 0;
}
