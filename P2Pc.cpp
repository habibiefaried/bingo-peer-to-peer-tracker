#include <iostream>
#include <cstring>
#include "ClientSocket.h"
#include "SocketException.h"
#include "ServerSocket.h"
#include "SocketException.h"
#include "Konversi.h"
#include "game.h"

int RangeNilai;
int LebarBoard;

bool IsDone = false;
int TebakanSaya;
int TebakanLawan;

int main()
{
	Bingo B;
	string IPTracker, IPPrev, IPNext, IPDest, my_IP;
	string PortPrev, PortNext, PortDest;
	
	string Command;
	string IPReply,Input;
	string Name;
	ClearScreen();
	string reply,status;
	string PortReply;
	string data1,data2;
	int col,row;
	bool isBoardOK = false;
	bool isGiliran = false;
	bool isSelesai = false;
	
	/* Tracker */
	cout<<"Masukkan Nama anda : "; cin>>Name;
	cout<<"Masukkan IP Address tracker : "; cin>>IPTracker;
	
	try
	{
		ClientSocket connect_socket (IPTracker,5051);
	
		connect_socket << Name; //memberikan Nama
		connect_socket >> reply; //menunggu reply dari server
	
		cout<<reply<<endl; //menuliskan reply dari server
		  	
		//cout<<"ketikkan sesuatu untuk memutuskan koneksi : "; cin>>S;
		connect_socket << "Client";
		connect_socket << "-1";
		ClearScreen();
		cout<<"List Of Server yang aktif"<<endl;
		cout<<"===================================================="<<endl;
		cout<<"ID   Nama"<<endl;
		cout<<"===================================================="<<endl;
		while (reply != "SELESAI")
		{
			connect_socket >> reply; //menunggu reply server yang aktif
		
			if (reply != "SELESAI") //jika reply bukan flag "Selesai"
			{
				cout << reply << endl; //menuliskan reply dari server
				connect_socket << "OK"; //memberikan feedback "OK"
			}
		}
		cout<<"===================================================="<<endl;
		cout<<"Masukkan ID Server (atau -1 untuk refresh) : "; cin >> Command;
		connect_socket << Command; 
		
		reply = "INIT";
		
		while (Command == "-1")
		{
			ClearScreen();
			cout<<"List Of Server yang aktif"<<endl;
			cout<<"===================================================="<<endl;
			cout<<"ID   Nama"<<endl;
			cout<<"===================================================="<<endl;
			while (reply != "SELESAI")
			{
				connect_socket >> reply; //menunggu reply server yang aktif
		
				if (reply != "SELESAI") //jika reply bukan flag "Selesai"
				{
					cout << reply << endl; //menuliskan reply dari server
					connect_socket << "OK"; //memberikan feedback "OK"
				}
			}
			cout<<"===================================================="<<endl;
			cout<<"Masukkan ID Server (atau -1 untuk refresh) : "; cin >> Command;
			connect_socket << Command; 
			reply = "INIT"; //reply jangan berisi nilai "SELESAI"
		}
		
		connect_socket >> IPReply; //dapat IP Address Jawaban 
		connect_socket >> PortReply; //dapat port jawaban
		connect_socket << "OKE";
		connect_socket >> my_IP;
		cout<<"IP Address anda : "<<my_IP<<endl;
		
		if ((IPReply == "0") || (PortReply == "0"))
			cout<<"Error!, server tidak ditemukan "<<endl;
		else
		{
			try //ngebind port dan IP ke Peer sebelumnya
			{
				ClientSocket prev_socket(IPReply,StringToInt(PortReply));
				cout<<"iseng"<<endl;
				IPPrev = IPReply; //IP Sebelumnya adalah 
				PortPrev = PortReply; //Port sebelumnya adalah port yang diberikan oleh tracker	
				
				prev_socket >> IPDest; // menerima IPAddress tujuan
				prev_socket << "OKE"; //kasih data dummy
				prev_socket >> PortDest; // menerima port tujuan
				
				cout<<"Data diterima. Port tujuan : "<<PortDest<<", IP Tujuan : "<<IPDest<<endl;
				
				if ((StringToInt(PortReply)+1) == StringToInt(PortDest)) //jika portreply + 1 == portdest
				{
					sleep(1);
					ClientSocket next_socket(IPDest,StringToInt(PortDest));
					cout<<"Terkoneksi dengan parent"<<endl;
					connect_socket << "QUIT"; //flags buat quit
					
					/* Main algoritma ada disini */
					Initialize(B);
					ClearScreen();
					
					prev_socket >> data1;
					LebarBoard = StringToInt(data1);
					prev_socket << "OK";
					prev_socket >> data2;
					RangeNilai = StringToInt(data2);
					
					next_socket << data1;
					next_socket >> reply;
					next_socket << data2;
					
					//pasang board
					cout << "Sedang menunggu lawan. . "<<endl;
					prev_socket >> data1; //menunggu sebuah data apapun
					prev_socket << "OK"; //reply
					FasaMulai(B);
					next_socket << "YOU";		
					next_socket >> reply;
					//end of pasang board
					
					while (!IsDone)
					{
						ClearScreen();
						PrintBingo(B); //Print Board bingo
						
						if (isGiliran)
						{
							cout<<"Nilai yang anda minta : "; cin>>TebakanSaya;
							
							if (SearchNumberLocation(B,TebakanSaya,col,row)) 
							//mencari nomor tebakan sendiri di board
							{
								B.a[row][col] = Marked;
							}
							
							next_socket << IntToString(TebakanSaya);
							next_socket >> reply;
							next_socket << "YOU";
							next_socket >> reply;
							
							if (CountXAll(B) >= Menang) //jika menang
							{
								ClearScreen();
								cout<<"Selamat, anda memenangkan pertandingan "<<endl;
								next_socket << Name;
								cout<<"Anda akan diredirect ke menu utama dalam 3 detik. . ."<<endl;
								prev_socket >> reply;
								prev_socket << "OK";
								prev_socket >> reply;
								prev_socket << "OK";
								prev_socket >> status; //penentuan menang atau kalah
								sleep(3);
								IsDone = true;
							}
							else
							{
								next_socket << "0"; //penentuan menang atau kalah
								prev_socket >> reply;
								prev_socket << "OK";
								prev_socket >> reply;
								prev_socket << "OK";
								prev_socket >> status; //penentuan menang atau kalah
							
								if (status != "0")
								{
									ClearScreen();
									cout<<"Maaf, anda kalah dalam pertandingan ini "<<endl;
									cout<<"Pemenangnya adalah "<<status<<endl;
									cout<<"Anda akan diredirect ke menu utama dalam 3 detik. . ."<<endl;
									sleep(3);
									IsDone = true;
								}
							}
							
							isGiliran = false;
							
						}
						else //jika bukan gilirannya
						{
							cout<<"Menunggu pilihan lawan . . ."<<endl;
							prev_socket >> reply;
							TebakanLawan =  StringToInt(reply); //menerjemahkan tebakan lawan
							if (SearchNumberLocation(B,TebakanLawan,col,row))
							//mencari tebakan lawan pada board bingo kita
							{
								B.a[row][col] = Marked;
							}
							prev_socket << "OK";
							prev_socket >> reply;
							prev_socket << "OK";
							prev_socket >> status; //penentuan menang atau kalah
							
							if (status != "0")
							{
								ClearScreen();
								cout<<"Maaf, anda kalah dalam pertandingan ini "<<endl;
								cout<<"Pemenangnya adalah "<<status<<endl;
								cout<<"Anda akan diredirect ke menu utama dalam 3 detik. . ."<<endl;
								IsDone = true;
							}
							
							
							if (reply == "YOU")
								isGiliran = true;
								
							next_socket << IntToString(TebakanLawan);
							next_socket >> reply;
							next_socket << "Not";
							next_socket >> reply;
							if (status != "0")
							{
								next_socket << status;
								sleep(3);
							}
							else
							{
								next_socket << "0";
							}
						}		
					}
							
					cout<<"Sukses"<<endl;
					/* End of Main */
				}
				else
				{
					sleep(1);
					ServerSocket server(StringToInt(PortReply)+1);
					ServerSocket next_socket;
				
					connect_socket << Command; //Nomor Server
					connect_socket >> reply; // menerima reply
					connect_socket << my_IP; //memberi IP
					connect_socket >> reply; //menerima reply
					
					connect_socket << IntToString(StringToInt(PortReply)+1);
					connect_socket >> reply; //menerima reply
					connect_socket << "QUIT"; //flags buat quit
					// Nomor Server : Command
					// IP Address kita : my_IP
					
					cout<<"Listening On Next Peer @ "<<(StringToInt(PortReply))+1<<endl;
					server.accept(next_socket);
					
					next_socket << IPDest; //memberi IPAddress Tujuan
					next_socket >> reply; //reply OKE
					next_socket << PortDest; //memberi port tujuan
					
					/* Main algoritma ada disini */
					Initialize(B);
					ClearScreen();
					
					prev_socket >> data1;
					LebarBoard = StringToInt(data1);
					prev_socket << "OK";
					prev_socket >> data2;
					RangeNilai = StringToInt(data2);
					
					next_socket << data1;
					next_socket >> reply;
					next_socket << data2;
					
					//pasang board
					cout << "Sedang menunggu lawan. . "<<endl;
					prev_socket >> data1; //menunggu sebuah data apapun
					prev_socket << "OK"; //reply
					FasaMulai(B);
					next_socket << "YOU";		
					next_socket >> reply;
					//end of pasang board
					
					while (!IsDone)
					{
						ClearScreen();
						PrintBingo(B); //Print Board bingo
						
						if (isGiliran)
						{
							cout<<"Nilai yang anda minta : "; cin>>TebakanSaya;
							
							if (SearchNumberLocation(B,TebakanSaya,col,row)) 
							//mencari nomor tebakan sendiri di board
							{
								B.a[row][col] = Marked;
							}
							
							next_socket << IntToString(TebakanSaya);
							next_socket >> reply;
							next_socket << "YOU";
							next_socket >> reply;
							
							if (CountXAll(B) >= Menang) //jika menang
							{
								ClearScreen();
								cout<<"Selamat, anda memenangkan pertandingan "<<endl;
								next_socket << Name;
								cout<<"Anda akan diredirect ke menu utama dalam 3 detik. . ."<<endl;
								prev_socket >> reply;
								prev_socket << "OK";
								prev_socket >> reply;
								prev_socket << "OK";
								prev_socket >> status; //penentuan menang atau kalah
								sleep(3);
								IsDone = true;
							}
							else
							{
								next_socket << "0"; //penentuan menang atau kalah
								prev_socket >> reply;
								prev_socket << "OK";
								prev_socket >> reply;
								prev_socket << "OK";
								prev_socket >> status; //penentuan menang atau kalah
							
								if (status != "0")
								{
									ClearScreen();
									cout<<"Maaf, anda kalah dalam pertandingan ini "<<endl;
									cout<<"Pemenangnya adalah "<<status<<endl;
									cout<<"Anda akan diredirect ke menu utama dalam 3 detik. . ."<<endl;
									sleep(3);
									IsDone = true;
								}
							}
							
							isGiliran = false;
							
						}
						else //jika bukan gilirannya
						{
							cout<<"Menunggu pilihan lawan . . ."<<endl;
							prev_socket >> reply;
							TebakanLawan =  StringToInt(reply); //menerjemahkan tebakan lawan
							if (SearchNumberLocation(B,TebakanLawan,col,row))
							//mencari tebakan lawan pada board bingo kita
							{
								B.a[row][col] = Marked;
							}
							prev_socket << "OK";
							prev_socket >> reply;
							prev_socket << "OK";
							prev_socket >> status; //penentuan menang atau kalah
							
							if (status != "0")
							{
								ClearScreen();
								cout<<"Maaf, anda kalah dalam pertandingan ini "<<endl;
								cout<<"Pemenangnya adalah "<<status<<endl;
								cout<<"Anda akan diredirect ke menu utama dalam 3 detik. . ."<<endl;
								IsDone = true;
							}
							
							
							if (reply == "YOU")
								isGiliran = true;
								
							next_socket << IntToString(TebakanLawan);
							next_socket >> reply;
							next_socket << "Not";
							next_socket >> reply;
							if (status != "0")
							{
								next_socket << status;
								sleep(3);
							}
							else
							{
								next_socket << "0";
							}
						}		
					}
							
					cout<<"Sukses"<<endl;
					/* End of Main */
				}
			}
			catch (SocketException &e)
			{
				cout<<"Program telah menangkap eksepsi : "<<e.description()<<"\n";
			}
			/* End of Tracker */
		}
	}
	catch (SocketException& e)
	{
		cout<<"Program telah menangkap eksepsi : "<<e.description()<<"\n";
	}
	return 0;
}
