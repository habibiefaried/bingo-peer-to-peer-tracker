// SocketException class


#ifndef SocketException_class
#define SocketException_class

#include <string>

class SocketException
{
	//Untuk menangani error2 yang terjadi
	public:
	SocketException (std::string s) : m_s (s) //Construktor initialization list
	{};
	~SocketException ()
	{};
	std::string description() //get description
	{ 
		return m_s;
	}
	
private:
	std::string m_s;
};

#endif
