#ifndef _Konversi_H
#define _Konversi_H
#include <iostream>
#include <cstring>

using namespace std;

typedef string infotype;

int CharToInt(char);
string IntToChar(int);
int StringToInt(string);
string IntToString(int I);
void StringToChar(string,char[100]);
#endif
